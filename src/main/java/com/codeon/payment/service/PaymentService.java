package com.codeon.payment.service;

import com.codeon.payment.entity.Payment;
import com.codeon.payment.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepository repository;

    public Payment doPayment(Payment payment) {
        payment.setPaymentStatus("success");
        payment.setTransactionId(UUID.randomUUID().toString());
        return repository.save(payment);
    }

    public List<Payment> getPaymentByOrderId(String id) {
        return repository.findAllByOrderId(id);
    }
}

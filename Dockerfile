FROM openjdk:11
ARG JAR_FILE=build/libs/payment-0.0.1.jar
COPY ${JAR_FILE} payment.jar
ENTRYPOINT ["java","-jar","/payment.jar"]